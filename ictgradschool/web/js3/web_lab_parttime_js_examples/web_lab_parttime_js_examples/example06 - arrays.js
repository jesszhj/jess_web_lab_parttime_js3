"use strict";

// an empty array
var my_array1 = new Array();

// an empty array using literal notation
var my_array2 = [];

// an array of known size but no data yet
var my_array3 = new Array(3);

// an array of known values
var my_array4 = new Array("COMP233", "COMP333", "COMP329");

// an array of known values (literal syntax)
var my_array = ["COMP233", "COMP333", "COMP329"];

// Getting the length of an array
var len = my_array.length;

// Getting elements
var comp233 = my_array[0]; // Zero-based index.
var comp333 = my_array[1]; // etc...

// Setting elements
my_array[0] = "Changed COMP233 to this string...";
my_array[3] = "Added a new element (index 3 is the fourth position, and there were only three elements before).";
my_array[6] = "Added another element. Left some blank spaces in-between.";
my_array[my_array.length] = "Added a new element right onto the end. This will work no matter how big the array is.";

// Looping through arrays with all numerical indices
for (var i = 0; i < my_array.length; i++) {
    var element = my_array[i];
    console.log("Element " + i + " = " + element);
}


// Sorting - alphabetically
var alphaArray = ["D", "A", "C", "B"];
console.log("Unsorted array of strings: " + alphaArray.toString()); // array's toString() function will produce a nice representation of the array for easy printing to the console.
alphaArray.sort();
console.log("Sorted array of strings: " + alphaArray.toString());

// Sorting - numerically
var intArray = [35, 3, 338];

// What happens with this? Why? Uncomment this line and comment the other sorting line to experiment!
// intArray.sort();

// The sort function will use this provided comparison function to do its sorting, rather than its default behavior.
// Experiment: What happens when you use b - a instead?
//intArray.sort(function(a, b) {return a - b; });
console.log("Sorted array of ints: " + intArray.toString());

// Associative Arrays
// These are arrays with strings as indices instead. Basically,
// behind the scenes the array is converted to a different type
// of array, and you can do different things with it.
// This example shows how we can use it as a "phone book", to find
// the phone number of a person.

// Storing values
var phoneBook = [];
phoneBook["Anne"] = 1234567;
phoneBook["Bob"] = 2345678;
phoneBook["Caitlin"] = 3456789;
phoneBook["Dave"] = 4567890;

// Getting values
var caitlinNumber = phoneBook["Caitlin"];
console.log("Caitlins number is: " + caitlinNumber);
console.log("Dave's number is: " + phoneBook["Dave"]);

// Looping through all values
// the "for...in" syntax will loop through each index value.
// So in this case - the people's names.
console.log("Phone Directory:");
for (var name in phoneBook) {

    console.log("- " + name + ": " + phoneBook[name]);

}