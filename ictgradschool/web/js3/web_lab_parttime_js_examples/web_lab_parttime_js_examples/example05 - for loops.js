"use strict";

var sum = 0;

// This for-loop is the same as the while loop in the previous example.
for (var i = 0; i < 5; i++) {
    sum += i;
    console.log(i);
}

console.log(sum);