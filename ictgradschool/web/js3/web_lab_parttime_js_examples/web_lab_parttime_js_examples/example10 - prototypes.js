"use strict";

// Defining a prototype for a person.
// These are useful when you want multiple objects all with the same
// properties (e.g. multiple people).
function Person(title, firstName, lastName, age) {
    this.title = title;
    this.firstName = firstName;
    this.lastName = lastName;
    this.age = age;
    this.getFullName = function() {
        return this.title + " " + this.firstName + " " + this.lastName;
    }
}

// Creating instances of our prototype. Note use of the "new" keyword.
var nick = new Person("Dr.", "Nick", "Riviera", 42);
var bob = new Person("Mr.", "Bob", "Builder", 47);

// Once created, the objects can be used exactly the same as JSON objects.
console.log(nick.getFullName());
console.log(bob.lastName);