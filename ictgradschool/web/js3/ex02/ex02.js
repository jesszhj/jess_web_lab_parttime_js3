"use strict";

// TODO Car

var car = {
    year: "2007",
    make: "BMW",
    model: "323i",
    bodyType: "Sedan",
    transmission: "Tiptronic",
    odometer: "68,512",
    price: "$16,000"
};

var swift = {
    title: "1989",
    artist: "Taylor Swift",
    year: "2014",
    genre: "pop",
    tracks: ["Welcome to New York", "Blank Space","Style","Out of the Woods","All You Had to Do Was Stay","Shake It Off",
    "I Wish You Would","Bad Blood","Wildest Dreams"," How You Get the Girl","This Love","I Know Places","Clean"]
   
};

function toString(a){
    return " Album: " + a.title + ", released in " + a.year + " by " + a["artist"] + " ";
}
// this serves like a formula where "a" is just a random variable -- Jess notes
console.log(toString(swift));
// function Car(year, make, model, bodyType, transmission, odometer, price) {
//     this.year = year;
//     this.make = make;
//     this.model = model;
//     this.bodyType = bodyType;
//     this.transmission = transmission;
//     this.odometer = odometer;
//     this.price = price;
//     this.getFullInfo = function () {
//         return "The car was made in " + this.year + ", that is a " + this.make + " " + this.model + " " + this.bodyType + " " +
//             this.transmission + " with odometer of " + this.odometer + " at price of " + this.price + ".";
//     }
// }
// var car1 = new Car("2007", "BMW", "323i", "Sedan", "Tiptronic", "68,512", "$16,000");
// console.log(car1.getFullInfo());
// // TODO Music


// function Music(title, artist, year, genre, tracks) {
//     this.title = title;
//     this.artist = artist;
//     this.year = year;
//     this.genre = genre;
//     this.tracks = tracks;
//     this.toString = function () {
//         return " Album: " + title + ", released in " + year + " by " + artist + " "
//     }
// };

// var music1 = new Music("1989", "Taylor Swift", "2014", "Pop", " ");
